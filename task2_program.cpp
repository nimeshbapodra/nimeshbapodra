#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

bool is_invalid(char a)
{
    if (!isdigit(a))
        return (true);
    else
        return (false);
}

void get_value (int &val)
{
    string input;
    cin >> input;

    /* If any character of input string is not digit then take input again. */
    while (any_of(input.begin(), input.end(), is_invalid))
    {
        cout << "Invalid input" << endl;
        cout << "Enter new value: ";
        cin >> input;
    }

    /* Convert input string to integer. */
    val = stoi(input);
}
 
void main()
{
    int num;
    int range_from;
    int range_to;
    int begin;
    int mul;
    bool above_range;
    vector<int> multiples;

    cout << "Program to print multiples in reverse order." << endl << endl;
    cout << "Instructions: Enter only positive integers."  << endl << endl;

start:

    begin = 1;
    above_range = false;

    cout << "Enter number: ";
    get_value (num);

    cout << "Enter range" << endl;

    cout << "From: ";
    get_value (range_from);

    cout << "To: ";
    get_value (range_to);

    if (num == 0)
    {
        /* Separately handle 0 given as input number to find multiples. */
        if (range_from == 0)
            cout << "All multiples of 0 are 0." << endl;
        else
            cout << "Could not find multiples in given range" << endl;
    }
    else
    {
        /* Here we try to find the first multiplying factor for which the multiple
           falls in given range. e.g. number is 7 and range is from 200 to 300.
           Then 29 is obtained as begin (29 * 7 = 203). This improves efficiency. */
        if (range_from > num)
        {
            begin = range_from / num;

            if (range_from % num > 0)
                begin++;
        }

        /* Find multiples until the range is not exceeded and store in a vector. */
        for (int i = begin; !above_range; i++)
        {
            mul = num * i;

            if (mul <= range_to)
                multiples.push_back(mul);
            else
                above_range = true;
        }

        if (multiples.empty())
            cout << "Could not find multiples in given range" << endl;
        else
            cout << "Multiples of " << num << " are: " << endl;

        /* Print numbers in reverse order and empty the vector. */
        while (!multiples.empty())
        {
            cout << multiples.back() << endl;
            multiples.pop_back();
        }
    }

    system("pause");

    goto start;
}