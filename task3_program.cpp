#include <iostream>
#include "sqlite3.h"
 
using namespace std;

static int display_table(void *NotUsed, int n_columns, char **value, char **column_name)
{
   for (int i = 0; i < n_columns; i++)
      cout << column_name[i] << " = " << value[i] << endl;

   cout << endl;
   return 0;
}
 
int main()
{
   int rc;
   char *error;
   char *sql;
   sqlite3 *db;

   if (sqlite3_threadsafe())
   {
       // sqlite3_threadsafe() returns true, means library was compiled in multi-thread or serialized mode.
       // Set the config mode as serialized in order to use single database connection across all threads.
       sqlite3_config(SQLITE_CONFIG_SERIALIZED);
   }
   else
   {
       // sqlite3_threadsafe() returns false, means library was compiled in single-thread mode.
       cout << "Unable to proceed further. Compile library in serialized mode and try again.";
       system ("pause");
       return 0;
   }
 
   // Open Database
   rc = sqlite3_open("Employee.db", &db);

   if (rc != SQLITE_OK)
   {
      cerr << "Error opening database: " << sqlite3_errmsg(db) << endl << endl;
      system ("pause");
      sqlite3_close(db);
      return 0;
   }
 
   // Create table
   sql = "CREATE TABLE EMPLOYEE (ID INTEGER PRIMARY KEY NOT NULL, NAME TEXT NOT NULL);";

   rc = sqlite3_exec(db, sql, NULL, NULL, &error);

   if (rc != SQLITE_OK)
   {
      cerr << "Error creating table: " << error << endl << endl;
      sqlite3_free(error);
      system ("pause");
      return 0;
   }

   // Insert commands are enclosed in between BEGIN TRANSACTION and COMMIT TRANSACTION
   // commands in order to execute them together. This improves performance.

   sqlite3_exec(db, "BEGIN TRANSACTION;", NULL, NULL, NULL);
 
   // Insert values in table
   sql = "INSERT INTO EMPLOYEE VALUES(1, 'A'); \
          INSERT INTO EMPLOYEE VALUES(2, 'B'); \
          INSERT INTO EMPLOYEE VALUES(3, 'C');";

   rc = sqlite3_exec(db, sql, NULL, NULL, &error);

   sqlite3_exec(db, "COMMIT TRANSACTION;", NULL, NULL, NULL);

   if (rc != SQLITE_OK)
   {
      cerr << "Error inserting values: " << error << endl << endl;
      sqlite3_free(error);
      system ("pause");
      return 0;
   }

   // Display table
   sql = "SELECT * FROM EMPLOYEE;";

   rc = sqlite3_exec(db, sql, display_table, NULL, &error);

   if (rc != SQLITE_OK)
   {
      cerr << "Error displaying table: " << error << endl << endl;
      sqlite3_free(error);
      system ("pause");
      return 0;
   }

   // Delete table
   sql = "DROP TABLE EMPLOYEE;";

   rc = sqlite3_exec(db, sql, NULL, NULL, &error);

   if (rc != SQLITE_OK)
   {
      cerr << "Error deleting table: " << error << endl << endl;
      sqlite3_free(error);
      system ("pause");
      return 0;
   }
 
   // Close database
   rc = sqlite3_close(db);

   if (rc != SQLITE_OK)
   {
      cerr << "Error closing table: " << error << endl << endl;
      sqlite3_free(error);
      system ("pause");
      return 0;
   }
   
   return 0;
}
